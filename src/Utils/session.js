export const setSession = (authResult) => {
  // Set the time that the access token will expire at
  // let expiresAt = JSON.stringify(
  //   authResult.expiresIn * 1000 + new Date().getTime()
  // );
  sessionStorage.setItem("token", authResult.token);
  sessionStorage.setItem("email", authResult.email);
  sessionStorage.setItem("user_level", authResult.user_level);
};

export const removeSession = () => {
  sessionStorage.removeItem("token");
  sessionStorage.removeItem("email");
  sessionStorage.removeItem("user_level");
};

export const setBeds = (num) => {
  sessionStorage.setItem("beds", num);
};

