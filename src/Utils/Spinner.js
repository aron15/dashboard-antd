import React, { Fragment } from "react";
import loader from "./spinner.gif";

const Spinner = () => {
  return (
    <Fragment>
      <img
        src={loader}
        style={{ width: "150px", margin: "auto", display: "block" }}
        alt="Loading..."
      />
    </Fragment>
  );
};

export default Spinner;
