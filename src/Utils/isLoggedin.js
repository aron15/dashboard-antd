import React from "react";
import { Route, Redirect } from "react-router-dom";

function isLoggedin({ component: Component, ...rest }) {
  return (
    <div>
      <Route
        {...rest}
        render={props => {
          // const user = localStorage.getItem("user_level");
          const check = false;
          // console.log(user, "usrr");
          if (check) {
            return <Component {...props} />;
          } else {
            return (
              <Redirect
                to={{
                  pathname: "/",
                  state: {
                    from: props.location
                  }
                }}
              />
            );
          }
        }}
      />
    </div>
  );
}

export default isLoggedin;
