import React from "react";
import ReactDOM from "react-dom";
import App from "./App";

//Styles
import "antd/dist/antd.css";
import "./Styles/App.scss";

//Initialize Redux
import { Provider } from "react-redux";
import store from "./Redux/store";

import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Routes from "./Routes";

ReactDOM.render(
  <Provider store={store}>
    <Router>
      <Routes />
    </Router>
  </Provider>,
  document.getElementById("root")
);
