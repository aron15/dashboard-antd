import React, { useState } from "react";
import { FormControl, InputLabel, MenuItem, Select } from "@material-ui/core";

function PlaceDropdown() {
  const [place, setPlace] = useState(1);

  function handleSelect(e) {
    const value = e.target.value;
    setPlace(value);
  }
  return (
    <div className="month-dropdown">
      <FormControl variant="outlined" className="formControl">
        <InputLabel id="demo-simple-select-outlined-label">Place</InputLabel>
        <Select
          className="dropdown-width"
          labelWidth={55}
          value={place}
          onChange={handleSelect}
        >
          <MenuItem value={1}>Goa</MenuItem>
          <MenuItem value={2}>Himachal Pradesh</MenuItem>
          <MenuItem value={3}>Kerala</MenuItem>
          <MenuItem value={4}>Punjab</MenuItem>
          <MenuItem value={5}>Rajasthan</MenuItem>
          <MenuItem value={6}>Uttarakhand</MenuItem>
        </Select>
      </FormControl>
    </div>
  );
}

export default PlaceDropdown;
