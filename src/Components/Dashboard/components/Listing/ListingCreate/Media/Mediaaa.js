import React, { useState } from "react";
import { useDispatch } from "react-redux";
import { Row, Col, Card, Skeleton, message } from "antd";
import { Upload, Modal } from "antd";
import { PlusOutlined } from "@ant-design/icons";
import { Button } from "@material-ui/core";
import { StepperMediaCreate } from "../../../../../../Redux/actions/stepper_actions";

function getBase64(file) {
  return new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => resolve(reader.result);
    reader.onerror = (error) => reject(error);
  });
}

const Image = () => {
  const dispatch = useDispatch();

  const [state, setState] = useState({
    previewVisible: false,
    previewImage: "",
    fileList: [],
  });

  const handleCancel = () => setState({ previewVisible: false });

  const handlePreview = async (file) => {
    if (!file.url && !file.preview) {
      file.preview = await getBase64(file.originFileObj);
    }
    setState({
      previewImage: file.url || file.preview,
      previewVisible: true,
    });
  };

  const handleChange = ({ fileList }) => setState({ fileList });
  const { previewVisible, previewImage, fileList } = state;

  const uploadButton = (
    <div>
      <PlusOutlined />
      <div className="ant-upload-text">Upload</div>
    </div>
  );

  const hndleSubmit = () => {
    const formData = new FormData();
    formData.append("step", 3);
    for (let file of fileList) {
      formData.append("upl", file);
    }
    dispatch(StepperMediaCreate(formData));
  };

  return (
    <Card style={{ margin: "15px", borderRadius: "5px" }}>
      {" "}
      <div className="clearfix">
        <Upload
          action="https://www.mocky.io/v2/5cc8019d300000980a055e76"
          listType="picture-card"
          fileList={fileList}
          onPreview={handlePreview}
          onChange={handleChange}
        >
          {fileList.length >= 8 ? null : uploadButton}
        </Upload>
        <Modal visible={previewVisible} footer={null} onCancel={handleCancel}>
          <img alt="example" style={{ width: "100%" }} src={previewImage} />
        </Modal>
      </div>
      <Button type="primary" htmlType="submit" onClick={hndleSubmit}>
        Submit
      </Button>{" "}
    </Card>
  );
};

export default Image;
