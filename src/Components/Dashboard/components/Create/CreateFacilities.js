import React, { useState, Fragment, useEffect } from "react";
import axios from "axios";
import { withRouter } from "react-router-dom";
import { Row, Col, Card, Form, Input, Typography, message, Button } from "antd";

const { Text, Title } = Typography;

const CreateFacilities = ({ history }) => {
  const [buttonLoading, setButtonLoading] = useState(false);
  const token = sessionStorage.getItem("token");
  const [data, setData] = useState([{}]);

  useEffect(() => {
    getFacilities();
  }, []);

  const getFacilities = async () => {
    const config = {
      headers: {
        "Content-Type": "application/json",
        "Access-Control-Allow-Origin": "*",
        token,
      },
    };
    try {
      const query = await axios.get(
        "http://18.217.42.86:8000/api/v1/private/listing/facilities",
        config
      );
      const res = await query.data;
      console.log(res, "res");
      setData(res.facilities);
    } catch (error) {
      console.log(error);
    }
  };

  const onFinish = async (values) => {
    setButtonLoading(true);
    const config = {
      headers: {
        "Access-Control-Allow-Origin": "*",
        "Content-Type": "application/json",
        token,
      },
    };

    try {
      const res = await axios.post(
        "http://18.217.42.86:8000/api/v1/private/listing/facilities/create",
        values,
        config
      );
      const data = await res.data;
      if (data.status === 200) {
        message.success("Facilities added successfully.");
      }
    } catch (err) {
      console.log(err);
      message.error("OOPS! Something went wrong.");
    }
    setButtonLoading(false);
  };

  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };
  return (
    <Fragment>
      <Card
        style={{ margin: "15px", borderRadius: "5px" }}
        title={
          <Title level={2} style={{ textAlign: "center" }}>
            <Text strong>Create Facilities</Text>
          </Title>
        }
        bordered={true}
      >
        <Row>
          <Col offset={8} lg={8}>
            <Form onFinish={onFinish} onFinishFailed={onFinishFailed}>
              <Form.Item
                name="name_value"
                rules={[
                  { required: true, message: "Please enter Facilities!" },
                ]}
              >
                <Input placeholder="Facilities" size="large" />
              </Form.Item>

              <Form.Item>
                <Button loading={buttonLoading} htmlType="submit">
                  Submit
                </Button>
              </Form.Item>
            </Form>
            <Col>
              <h1>List of Facilities</h1>
              {data.map((x, i) => (
                <div
                  key={i}
                  style={{
                    display: "flex",
                    justifyContent: "space-between",
                    alignItems: "center",
                  }}
                >
                  <p>
                    <b> {x.name} </b>
                  </p>
                  <p>{x.enabled ? "Active" : "InActive"}</p>
                </div>
              ))}
            </Col>
          </Col>
        </Row>
      </Card>
    </Fragment>
  );
};

export default withRouter(CreateFacilities);
