import React, { useState, Fragment, useEffect } from "react";
import axios from "axios";
import { withRouter } from "react-router-dom";
import {
  Row,
  Col,
  Card,
  Form,
  Typography,
  message,
  Button,
  Divider,
  Skeleton,
  AutoComplete,
  Select,
  Input,
} from "antd";

const { Text, Title } = Typography;
const { Option } = Select;

const OtaSync = ({ history }) => {
  const [buttonLoading, setButtonLoading] = useState(false);
  const [loading, setLoading] = useState(false);

  const [data, setData] = useState([]);
  const [ota, setOta] = useState([]);
  const [property, setProperty] = useState([]);
  const [form, setForm] = useState({
    ota_name: "",
    ota_id: "",
    path_to_property: "",
    property_id: "",
  });

  const token = sessionStorage.getItem("token");

  useEffect(() => {
    fetchAPI();
    getOtas();
  }, []);

  const fetchAPI = async () => {
    setLoading(true);
    const config = {
      headers: {
        "Content-Type": "application/json",
        "Access-Control-Allow-Origin": "*",
        token,
      },
    };
    try {
      const res = await axios.get(
        "http://18.217.42.86:8000/api/v1/private/misc/sync_master/list",
        config
      );
      const data = await res.data;
      setData(data);
      console.log(data);
    } catch (err) {
      console.log(err);
    }
    setLoading(false);
  };

  const getOtas = async () => {
    const config = {
      headers: {
        "Content-Type": "application/json",
        "Access-Control-Allow-Origin": "*",
        token,
      },
    };
    try {
      const res = await axios.get(
        "http://18.217.42.86:8000/api/v1/private/misc/ota_master/list",
        config
      );
      const data = await res.data;
      setOta(data);
    } catch (err) {
      console.log(err);
    }
  };

  const handleDelete = async (id) => {
    setLoading(true);
    const config = {
      headers: {
        "Content-Type": "application/json",
        "Access-Control-Allow-Origin": "*",
        token,
      },
    };
    try {
      const res = await axios.post(
        "http://18.217.42.86:8000/api/v1/private/misc/sync_master/delete",
        { sync_id: id },
        config
      );
      const data = await res.data;
      if (data.status === 200) {
        message.success("Successfully deleted OTA.");
        fetchAPI();
      } else {
        message.success("Error while deleting OTA.");
      }
    } catch (err) {
      console.log(err);
      message.success("Error while deleting OTA.");
    }
    setLoading(false);
  };

  const onFinish = async () => {
    setButtonLoading(true);
    console.log(form, "submitting");
    const config = {
      headers: {
        "Access-Control-Allow-Origin": "*",
        "Content-Type": "application/json",
        token,
      },
    };

    try {
      const res = await axios.post(
        "http://18.217.42.86:8000/api/v1/private/misc/sync_master",
        form,
        config
      );
      const data = await res.data;
      console.log(data);
      if (data.status === 200) {
        message.success("OTA added successfully.");
        fetchAPI();
      }
    } catch (err) {
      console.log(err);
      message.error("OOPS! Something went wrong.");
    }
    setButtonLoading(false);
  };

  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };

  const onChangeOTA = (name) => {
    let getOta_id = "";
    if (name) {
      getOta_id = ota.filter((x) => x.ota_name === name)[0].ota_id;
    }
    setForm({ ota_name: name, ota_id: getOta_id });
  };

  const onChangeProperty = async (name) => {
    try {
      const res = await axios.get(
        `http://18.217.42.86:8000/api/v1/public/listing/search/${name}`
      );
      const data = await res.data;
      console.log(data, "dfdaf");
      setProperty(data);
    } catch (err) {
      console.log(err);
    }

    const getId = property.filter((x) => x.property_title === name);
    const id = getId.length > 0 ? getId[0].listing_id : "";
    setForm({ ...form, property_id: id });
  };

  return (
    <Fragment>
      <Row
        style={{
          display: "flex",
          justifyContent: "center",
        }}
      >
        <Col lg={17}>
          <Card
            style={{ margin: "15px", borderRadius: "5px" }}
            title={
              <Title level={2} style={{ textAlign: "center" }}>
                <Text strong>OTA Sync</Text>
              </Title>
            }
            bordered={true}
          >
            <Row>
              <Col lg={24}>
                <Form
                  onFinish={onFinish}
                  layout="inline"
                  onFinishFailed={onFinishFailed}
                >
                  <Col lg={6} style={{ margin: "8px" }}>
                    <AutoComplete
                      style={{ width: "100%" }}
                      size="large"
                      placeholder="Select OTA to sync"
                      onChange={(e) => onChangeOTA(e)}
                      filterOption={(inputValue, option) =>
                        option.value
                          .toUpperCase()
                          .indexOf(inputValue.toUpperCase()) !== -1
                      }
                    >
                      {ota.map((x, i) => {
                        return (
                          <Option key={i} value={x.ota_name}>
                            {x.ota_name}
                          </Option>
                        );
                      })}
                    </AutoComplete>
                  </Col>
                  <Col lg={6} style={{ margin: "8px" }}>
                    <Input
                      size="large"
                      name="path_to_property"
                      placeholder="Enter path to property"
                      onChange={(e) =>
                        setForm({ ...form, path_to_property: e.target.value })
                      }
                    />
                  </Col>
                  <Col lg={6} style={{ margin: "8px" }}>
                    <AutoComplete
                      style={{ width: "100%" }}
                      size="large"
                      placeholder="Select Property"
                      onChange={onChangeProperty}
                      filterOption={(inputValue, option) =>
                        option.value
                          .toUpperCase()
                          .indexOf(inputValue.toUpperCase()) !== -1
                      }
                    >
                      {property.map((x, i) => {
                        return (
                          <Option key={i} value={x.property_title}>
                            {x.property_title}
                          </Option>
                        );
                      })}
                    </AutoComplete>
                  </Col>
                  <Form.Item style={{ margin: "8px" }}>
                    <Button
                      htmlType="submit"
                      size="large"
                      loading={buttonLoading}
                    >
                      Submit
                    </Button>
                  </Form.Item>
                </Form>
              </Col>
            </Row>
            <Divider />
            {loading ? (
              <Skeleton active />
            ) : (
              <Row>
                {data.map((x, i) => {
                  return (
                    <Col lg={24} key={i}>
                      <Row
                        style={{
                          padding: "1rem",
                          display: "flex",
                          alignItems: "center",
                        }}
                      >
                        <Col lg={6} style={{ fontSize: "15px" }}>
                          <Text> {x.ota_name}</Text>{" "}
                        </Col>

                        <Col lg={12}>
                          <Text>
                            {" "}
                            sync with : <b> {x.path_to_property} </b>
                          </Text>{" "}
                        </Col>
                        <Col
                          lg={6}
                          style={{
                            display: "flex",
                            justifyContent: "flex-end",
                          }}
                        >
                          <Button
                            danger
                            onClick={() => handleDelete(x.sync_id)}
                          >
                            Delete
                          </Button>
                        </Col>
                      </Row>
                    </Col>
                  );
                })}
              </Row>
            )}
          </Card>
        </Col>
      </Row>
    </Fragment>
  );
};

export default withRouter(OtaSync);
