import React from "react";
import { Row, Col } from "antd";
import Calendar from "../../components/Calendar";
import QuickListing from "../../components/QuickListing";
import Shortcut from "../../components/shortcut";
import Todo from "../../components/todoList";

const Home = () => {
  return (
    <div>
      <Row justify="space-around">
        <Col className="col" xs={{ span: 22 }} lg={{ span: 22 }}>
          <h1>House keeping Audit</h1>
          <h1>3250</h1>
        </Col>
      </Row>
      <Row>
        <Col xs={{ span: 24 }} lg={{ span: 24 }}>
          <Calendar />
        </Col>
      </Row>
    </div>
  );
};

export default Home;
